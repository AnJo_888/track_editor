extends Spatial

onready var started = false

func _ready() -> void:
	pass


func _process(_delta) -> void:
	if started:
		$Latch.free()
		started = false
