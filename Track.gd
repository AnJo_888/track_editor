extends Spatial


var _x = 0.0
var _y = 1000.0
var _yRot = 0
var _z = 0.0
var altCam = 1
#onready var ball1
#onready var ball2
onready var balls = []
var ballX = -19.0
var ballY = 1005.0
var ballZ = 1.5
var ballAdjX = 2.0
var ballAdjY = -0.4
var ballAdjZ = 3.0
onready var ball_resource = load("res://Ball.tscn")
var foo
onready var globals = get_node("/root/Globals")
var isPaused = false
var mySeed = null
var numBalls = 16
var partAnt = ""
var partDef = {"RC":load("res://RC.tscn"), "RM":load("res://RM.tscn"), "RL":load("res://RL.tscn"),
			   "CCD":load("res://CCD.tscn"), "CCE":load("res://CCE.tscn"), "CMD":load("res://CMD.tscn"),
			   "CME":load("res://CME.tscn"), "CLD":load("res://CLD.tscn"), "CLE":load("res://CLE.tscn")}
var partName =["RC", "RM", "RL", "CCD", "CCE", "CMD", "CME", "CLD", "CLE"]
var partRelDef = {"RCRC":  0, "RCRM":  1,
				  "RCCCD": 2, "RCCCE": 3,
				  "RMRC":  4, "RMRM":  5, "RMRL":  6,
				  "RMCMD": 7, "RMCME": 8,
				  "RLRM":  9, "RLRL":  10,
				  "RLCLD": 11, "RLCLE": 12,
				  "CCDRC": 13, "CCDCCD":14, "CCDCCE":15,
				  "CCERC": 16, "CCECCD":17, "CCECCE":18,
				  "CMDRM": 19, "CMDCMD":20, "CMDCME":21,
				  "CMERM": 22, "CMECMD":23, "CMECME":24,
				  "CLDRL": 25, "CLDCLD":26, "CLDCLE":27,
				  "CLERL": 28, "CLECLD":29, "CLECLE":30
				}
var partsRelTrans = [
					 Vector3(10.0, -1.13, 0.0), Vector3(15.1, -1.82, 0.0), # RC to RC, RM
					 Vector3(11.6, -1.13, 3.31), Vector3(11.6, -1.13, -3.31), # RC to CCD, CCE
					 Vector3(15.1, -1.81, 0.0), Vector3(15.1, -1.88, 0.0), Vector3(30.79, -5.05, 0.0), # RM to RC, RM, RL
					 Vector3(22.73, -3.47, 5.28), Vector3(22.75, -3.47, -7.3), # RM to CMD, CME
					 Vector3(30.0, -4.91, 0.0), Vector3(41.4, -7.58, 0.0), # RL to RM, RL
					 Vector3(49.76, -10.43, 14.84), Vector3(47.75, -10.43, -12.83), # RL to CLD, CLE
					 Vector3(11.58, -1.10, -3.36), Vector3(13.28, -1.09, 0.0), Vector3(13.28, -1.09, -6.69), # CCD to RC, CCD, CCE
					 Vector3(3.36, -1.08, -11.58), Vector3(13.28, -1.09, 6.69), Vector3(0.0, -1.09, -13.28), # CCE to RC, CCD, CCE	
					 Vector3(24.81, -3.48, -7.33), Vector3(27.4, -4.4, -2.0), Vector3(27.4, -4.4, -14.6), # CMD to RM, CMD, CME
					 Vector3(7.29, -3.48, -22.77), Vector3(25.37, -4.4, 12.59), Vector3(0.0, -4.4, -25.37), # CME to RM, CMD, CME
					 Vector3(45.78, -10.38, -10.85), Vector3(54.3, -13.21, 3.97), Vector3(52.22, -13.17, -23.71), # CLD to RL, CLD, CLE
					 Vector3(12.86, -10.38, -47.72), Vector3(56.22, -13.18, 27.64), Vector3(0.00, -13.18, -54.25) # CLE to RL, CLD, CLE
					]
var rng = RandomNumberGenerator.new()
onready var rot
var start = false
var time = Timer.new()
var totParts = 0
var toTest = true
onready var track = []
onready var vectorPart
onready var windowMaximized = false

func _ready() -> void:
	add_child(time)

func _process(_delta) -> void:
	if start:
		$Viewport1/Viewport/Camera1.target = balls[13].get_global_transform().origin
		$Viewport2/Viewport/Camera2.target = balls[13].get_global_transform().origin
	if Input.is_action_just_pressed("ui_accept") && toTest && !start:
		testa()
	if Input.is_action_just_pressed("ui_cancel") || Input.is_action_just_pressed("ui_end"):
		get_tree().quit()
	if Input.is_action_just_pressed("ui_home"):
		foo = get_tree().reload_current_scene()
		get_tree().paused = false
		start = false
	if Input.is_action_just_pressed("ui_page_down"):
		if !isPaused:
			get_tree().paused = true
		else:
			get_tree().paused = false
		isPaused = !isPaused
	if OS.window_maximized:
		if !windowMaximized:
			$Viewport1.set_margin(MARGIN_RIGHT, (OS.get_screen_size().x / 2))
			$Viewport1.set_margin(MARGIN_BOTTOM, OS.get_screen_size().y)
			$Viewport2.set_margin(MARGIN_LEFT, (OS.get_screen_size().x / 2))
			$Viewport2.set_margin(MARGIN_RIGHT, OS.get_screen_size().x)
			$Viewport2.set_margin(MARGIN_BOTTOM, OS.get_screen_size().y)
			$Viewport2.set_position(Vector2(OS.get_screen_size().x / 2, 0))
			windowMaximized = true
	else:
		if windowMaximized:
			$Viewport1.set_margin(MARGIN_RIGHT, (OS.window_size.x / 2))
			$Viewport1.set_margin(MARGIN_BOTTOM, OS.window_size.y)
			$Viewport2.set_margin(MARGIN_LEFT, (OS.window_size.x / 2))
			$Viewport2.set_margin(MARGIN_RIGHT, OS.window_size.x)
			$Viewport2.set_margin(MARGIN_BOTTOM, OS.window_size.y)
			$Viewport2.set_position(Vector2(OS.window_size.x / 2, 0))
			windowMaximized = false


func testa() -> void:
	random_track()
	time.start((2 + (rng.randi() % 2)))
	yield(time,"timeout")
	$Starting.started = true
	start = true
#	vai(["RC", "RC", "RM", "RM", "RL", "RL", "RM", "RC", "CCD", "CCD", "CCE",
#		 "CCD", "RC", "CCE", "CCE", "RC", "RM", "CMD", "CMD", "CME", "CMD", "RM",
#		 "CME", "CME", "RM", "RL", "CLD", "CLD", "CLE", "CLD", "RL", "CLE", "RL"], 1)


func random_track() -> void:
#	mySeed = -5906986863281101267
#	mySeed = -3260260479253510195
#	mySeed = -4868743819004158189
#	mySeed = 3770488293977654650
#	mySeed = 9163583218275098974
	if mySeed == null:
		rng.randomize()
	else:
		rng.set_seed(mySeed)
	print("	mySeed = ", rng.get_seed())
	var ant = "RL"
	var contParts = 1
	var seqCC = 0
	var seqCM = 0
	var seqCL = 0
	var limParts = 29 + (rng.randi() % 11)
	var lista = ["RL"]
	var maxCC = 1 # mais, eventualmente, uma outra CC
	var maxCM = 2 # mais, eventualmente, uma outra CM
	var maxCL = 3 # mais, eventualmente, uma outra CL
	var randPart = ""
	while contParts < limParts:
		randPart = partName[rng.randi() % 9]
		if randPart == "RC":
			if ant == "RC" || ant == "RM" || ant == "CCD" || ant == "CCE":
				lista.append(randPart)
				ant =randPart
				seqCC = 0
#				seqCM = 0
#				seqCL = 0
				contParts += 1
		elif randPart == "RM":
			if ant == "RC" || ant == "RM" || ant == "RL" || ant == "CMD" || ant == "CME":
				lista.append(randPart)
				ant =randPart
#				seqCC = 0
				seqCM = 0
#				seqCL = 0
				contParts += 1
		elif randPart == "RL":
			if ant == "RM" || ant == "RL" || ant == "CLD" || ant == "CLE":
				lista.append(randPart)
				ant =randPart
#				seqCC = 0
#				seqCM = 0
				seqCL = 0
				contParts += 1
		elif randPart == "CCD" || randPart == "CCE":
			if seqCC <= maxCC:
				if ant == "RC" || ant == "CCD" || ant == "CCE":
					lista.append(randPart)
					ant =randPart
					seqCC += 1
					contParts += 1
		elif randPart == "CMD" || randPart == "CME":
			if seqCM <= maxCM:
				if ant == "RM" || ant == "CMD" || ant == "CME":
					lista.append(randPart)
					ant =randPart
					seqCM += 1
					contParts += 1
		elif randPart == "CLD" || randPart == "CLE":
			if seqCL <= maxCL:
				if ant == "RL" || ant == "CLD" || ant == "CLE":
					lista.append(randPart)
					ant =randPart
					seqCL += 1
					contParts += 1
#		print("CC: ", seqCC, " - ","CM: ", seqCM, " - ","CL: ", seqCL)

	match ant:
		"RC":
			lista.append("RM")
			lista.append("RL") # Finish line
		"RM":
			lista.append("RL") # Finish line
		"RL":
			lista.append("RL") # Finish line
		"CCD":
			lista.append("RC")
			lista.append("RM")
			lista.append("RL") # Finish line
		"CCE":
			lista.append("RC")
			lista.append("RM")
			lista.append("RL") # Finish line
		"CMD":
			lista.append("RM")
			lista.append("RL") # Finish line
		"CME":
			lista.append("RM")
			lista.append("RL") # Finish line
		"CLD":
			lista.append("RL") # Finish line
		"CLE":
			lista.append("RL") # Finish line

	print(lista.size(), " partes")
	print (lista)
	vai(lista, 1)

func add_part(partChosen) -> void:
	var relParts = partChosen.insert(0, partAnt)
	var vectorBase = Vector3.ZERO
	var vectorSoma = Vector3.ZERO

	if totParts > 0:
#		print(relParts, " - ", _yRot, "º")
		vectorBase = partsRelTrans[partRelDef[relParts]]
		match _yRot:
			0:
				if partChosen.ends_with("D"):
					vectorSoma = vectorBase
				elif partChosen.ends_with("E"):
					if partAnt.ends_with("E"):
						vectorSoma.x = -vectorBase.z
						vectorSoma.y = vectorBase.y
						vectorSoma.z = vectorBase.x
					else:
						vectorSoma = vectorBase
				else:
					if partAnt.ends_with("E"):
						vectorSoma.x = -vectorBase.z
						vectorSoma.y = vectorBase.y
						vectorSoma.z = vectorBase.x
					else:
						vectorSoma = vectorBase
			90:
				if partChosen.ends_with("D"):
					if partAnt.ends_with("E"):
						vectorSoma.x = vectorBase.z
						vectorSoma.y = vectorBase.y
						vectorSoma.z = -vectorBase.x
					else:
						vectorSoma.x = vectorBase.z
						vectorSoma.y = vectorBase.y
						vectorSoma.z = -vectorBase.x
				elif partChosen.ends_with("E"):
					if partAnt.ends_with("E"):
						vectorSoma = vectorBase
					else:
						vectorSoma.x = vectorBase.z
						vectorSoma.y = vectorBase.y
						vectorSoma.z = -vectorBase.x
				else:
					if partAnt.ends_with("D"):
						vectorSoma.x = vectorBase.z
						vectorSoma.y = vectorBase.y
						vectorSoma.z = -vectorBase.x
					elif partAnt.ends_with("E"):
						vectorSoma = vectorBase
					else:
						vectorSoma.x = -vectorBase.z
						vectorSoma.y = vectorBase.y
						vectorSoma.z = -vectorBase.x
			180:
				if partChosen.ends_with("D"):
					if partAnt.ends_with("E"):
						vectorSoma.x = -vectorBase.x
						vectorSoma.y = vectorBase.y
						vectorSoma.z = -vectorBase.z
					else:
						vectorSoma.x = -vectorBase.x
						vectorSoma.y = vectorBase.y
						vectorSoma.z = -vectorBase.z
				elif partChosen.ends_with("E"):
					if partAnt.ends_with("E"):
						vectorSoma.x = vectorBase.z
						vectorSoma.y = vectorBase.y
						vectorSoma.z = vectorBase.x
					else:
						vectorSoma.x = -vectorBase.x
						vectorSoma.y = vectorBase.y
						vectorSoma.z = -vectorBase.z
				else:
					if partAnt.ends_with("E"):
						vectorSoma.x = vectorBase.z
						vectorSoma.y = vectorBase.y
						vectorSoma.z = -vectorBase.x
					else:
						vectorSoma.x = -vectorBase.x
						vectorSoma.y = vectorBase.y
						vectorSoma.z = -vectorBase.z
			270:
				if partChosen.ends_with("D"):
					if partAnt.ends_with("E"):
						vectorSoma.x = -vectorBase.z
						vectorSoma.y = vectorBase.y
						vectorSoma.z = vectorBase.x
					else:
						vectorSoma.x = -vectorBase.z
						vectorSoma.y = vectorBase.y
						vectorSoma.z = vectorBase.x
				elif partChosen.ends_with("E"):
					if partAnt.ends_with("E"):
						vectorSoma.x = vectorBase.x
						vectorSoma.y = vectorBase.y
						vectorSoma.z = -vectorBase.z
					else:
						vectorSoma.x = -vectorBase.z
						vectorSoma.y = vectorBase.y
						vectorSoma.z = vectorBase.x
				else:
					if partAnt.ends_with("E"):
						vectorSoma.x = -vectorBase.x
						vectorSoma.y = vectorBase.y
						vectorSoma.z = -vectorBase.z
					else:
						vectorSoma.x = -vectorBase.z
						vectorSoma.y = vectorBase.y
						vectorSoma.z = vectorBase.x
		vectorPart += vectorSoma

		track.append(partDef[partChosen].instance())
		track[(totParts - 1)].translation = vectorPart
		track[(totParts - 1)].rotate_object_local(Vector3(0,1,0), float(deg2rad(_yRot)))
		add_child(track[(totParts - 1)])
	#	print(track[(totParts - 1)].get_global_transform())
		if partChosen.ends_with("D"):
			_yRot += 270
		elif partChosen.ends_with("E"):
			_yRot += 90
		_yRot %= 360

	partAnt = partChosen
	totParts += 1


func vai(parts, qtde) -> void:
	if balls.size() > 0:
		for i in range(numBalls):
			remove_child(balls[i])
	for obj in track:
		remove_child(obj)
#	_yRot = rot
	partAnt = ""
	totParts = 0
	track = []
	vectorPart = Vector3(_x, _y, _z)
	print("------------")
	for part in parts:
		for _i in range(qtde):
			print(part)
			add_part(part)
	create_balls(numBalls)


#func create_balls() -> void:
#	ball1 = ball_resource.instance()
#	ball1.translation = Vector3(ballX, 1005.0, 1.5)
#	ball1.get_node("Body").set_surface_material(0, load(globals.ball_materials[(rng.randi() % 16)]))
#	add_child(ball1)
#	$Viewport1/Viewport/Camera1.target = ball1.get_global_transform().origin
#	$Viewport1/Viewport/Camera1.start = true
#	ball2 = ball_resource.instance()
#	ball2.translation = Vector3(ballX, 1005.0, -1.5)
#	ball2.get_node("Body").set_surface_material(0, load(globals.ball_materials[(rng.randi() % 16)]))
#	add_child(ball2)
#	$Viewport2/Viewport/Camera2.target = ball2.get_global_transform().origin
#	$Viewport2/Viewport/Camera2.start = true
#	start = true


func create_balls(qtde):
	var ballMultZ
	var escolhido = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	var mat
	var ok = false
	for i in range(qtde):
		balls.append(ball_resource.instance())
#		balls[i].scale = Vector3(0.47, 0.47, 0.47)
		if (i % 2) == 0:
			ballMultZ = 1
		else:
			ballMultZ = -1
		balls[i].translation = Vector3((ballX + (i * ballAdjX)),
									  (ballY + (i * ballAdjY)),
									  (ballZ + ((i % 2 * ballAdjZ) * ballMultZ)))
		while !ok:
			mat = rng.randi() % 16
			if escolhido[mat] == 0:
				balls[i].get_node("Body").set_surface_material(0, load(globals.ball_materials[mat]))
				escolhido[mat] = 1
				ok = true
		add_child(balls[i])
		ok = false
	$Viewport1/Viewport/Camera1.target = balls[13].get_global_transform().origin
	$Viewport1/Viewport/Camera1.start = true
	$Viewport2/Viewport/Camera2.target = balls[13].get_global_transform().origin
	$Viewport2/Viewport/Camera2.start = true
