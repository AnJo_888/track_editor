extends Camera

var angle = 17
var distance = 13
var height = 23
var target
var start = false

func _ready() -> void:
	set_as_toplevel(true)
	
	
func _physics_process(_delta: float) -> void:
	if start:
#		var target = get_parent().get_global_transform().origin
		var pos = get_global_transform().origin
		var up = Vector3(0, 1, 0)
		var offset = pos - target
		offset.y = sin(angle * PI / 180) * height
		offset = offset.normalized() * distance
		pos = target + offset
		look_at_from_position(pos, target, up)
